import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: ['error', 'debug', 'warn', 'log'] });
  app.enableCors({ origin: [
    'http://localhost:7400',
    'http://localhost:7200',
  ] });
  await app.listen(3000);
}
bootstrap();
