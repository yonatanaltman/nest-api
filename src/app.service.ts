import { Injectable, Logger } from '@nestjs/common';
const sleep = (time: number) => new Promise((resolve, reject) => setTimeout(resolve, time));
@Injectable()
export class AppService {
  async getHello(req?): Promise<{}> {
    const a = { rc: 0, message: 'success' };
    Logger.log('sleep!') ;
    await sleep(2000);
    Logger.log('wake up!') ;
    return a;
  }
}
