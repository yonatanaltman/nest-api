import { Controller, Get, Post, Logger } from '@nestjs/common';
import { AppService } from './app.service';

// my-domain/
/**
 * my-domain/users
 * @Controller('users)
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('api/order')
  getHello() {

    return this.appService.getHello();
  }
  @Post('api/order')
  postHello(req: any): {} {
    Logger.log(req);
    return this.appService.getHello(req);
  }
}
